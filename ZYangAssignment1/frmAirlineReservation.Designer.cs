﻿namespace ZYangAssignment1
{
    partial class frmAirlineReservation
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtState = new System.Windows.Forms.TextBox();
            this.btnState = new System.Windows.Forms.Button();
            this.btnAddWaitingList = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnBook = new System.Windows.Forms.Button();
            this.lstColumn = new System.Windows.Forms.ListBox();
            this.lstRow = new System.Windows.Forms.ListBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rtbShowWaitingList = new System.Windows.Forms.RichTextBox();
            this.rtbShowAll = new System.Windows.Forms.RichTextBox();
            this.btnShowWaitingList = new System.Windows.Forms.Button();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDebug = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtState);
            this.panel1.Controls.Add(this.btnState);
            this.panel1.Controls.Add(this.btnAddWaitingList);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnBook);
            this.panel1.Controls.Add(this.lstColumn);
            this.panel1.Controls.Add(this.lstRow);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(336, 26);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(314, 265);
            this.panel1.TabIndex = 1;
            // 
            // txtState
            // 
            this.txtState.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtState.Location = new System.Drawing.Point(213, 121);
            this.txtState.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtState.Name = "txtState";
            this.txtState.ReadOnly = true;
            this.txtState.Size = new System.Drawing.Size(81, 20);
            this.txtState.TabIndex = 8;
            // 
            // btnState
            // 
            this.btnState.Location = new System.Drawing.Point(213, 90);
            this.btnState.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnState.Name = "btnState";
            this.btnState.Size = new System.Drawing.Size(80, 27);
            this.btnState.TabIndex = 7;
            this.btnState.Text = "State";
            this.btnState.UseVisualStyleBackColor = true;
            this.btnState.Click += new System.EventHandler(this.btnState_Click);
            // 
            // btnAddWaitingList
            // 
            this.btnAddWaitingList.Location = new System.Drawing.Point(14, 216);
            this.btnAddWaitingList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAddWaitingList.Name = "btnAddWaitingList";
            this.btnAddWaitingList.Size = new System.Drawing.Size(176, 27);
            this.btnAddWaitingList.TabIndex = 6;
            this.btnAddWaitingList.Text = "Add To Waiting List";
            this.btnAddWaitingList.UseVisualStyleBackColor = true;
            this.btnAddWaitingList.Click += new System.EventHandler(this.btnAddWaitingList_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(110, 186);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 27);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnBook
            // 
            this.btnBook.Location = new System.Drawing.Point(14, 186);
            this.btnBook.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(80, 27);
            this.btnBook.TabIndex = 4;
            this.btnBook.Text = "Book";
            this.btnBook.UseVisualStyleBackColor = true;
            this.btnBook.Click += new System.EventHandler(this.btnBook_Click);
            // 
            // lstColumn
            // 
            this.lstColumn.FormattingEnabled = true;
            this.lstColumn.Items.AddRange(new object[] {
            "0",
            "1",
            "2"});
            this.lstColumn.Location = new System.Drawing.Point(110, 90);
            this.lstColumn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lstColumn.Name = "lstColumn";
            this.lstColumn.Size = new System.Drawing.Size(81, 82);
            this.lstColumn.TabIndex = 3;
            // 
            // lstRow
            // 
            this.lstRow.FormattingEnabled = true;
            this.lstRow.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.lstRow.Location = new System.Drawing.Point(14, 90);
            this.lstRow.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lstRow.Name = "lstRow";
            this.lstRow.Size = new System.Drawing.Size(81, 82);
            this.lstRow.TabIndex = 2;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(67, 25);
            this.txtName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(228, 20);
            this.txtName.TabIndex = 1;
            this.txtName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtName_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(347, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Booking and Cancellation";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.rtbShowWaitingList);
            this.panel2.Controls.Add(this.rtbShowAll);
            this.panel2.Controls.Add(this.btnShowWaitingList);
            this.panel2.Controls.Add(this.btnShowAll);
            this.panel2.Location = new System.Drawing.Point(46, 328);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(404, 268);
            this.panel2.TabIndex = 3;
            // 
            // rtbShowWaitingList
            // 
            this.rtbShowWaitingList.Location = new System.Drawing.Point(223, 48);
            this.rtbShowWaitingList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rtbShowWaitingList.Name = "rtbShowWaitingList";
            this.rtbShowWaitingList.ReadOnly = true;
            this.rtbShowWaitingList.Size = new System.Drawing.Size(158, 209);
            this.rtbShowWaitingList.TabIndex = 10;
            this.rtbShowWaitingList.Text = "";
            // 
            // rtbShowAll
            // 
            this.rtbShowAll.Location = new System.Drawing.Point(14, 48);
            this.rtbShowAll.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rtbShowAll.Name = "rtbShowAll";
            this.rtbShowAll.ReadOnly = true;
            this.rtbShowAll.Size = new System.Drawing.Size(158, 209);
            this.rtbShowAll.TabIndex = 9;
            this.rtbShowAll.Text = "";
            // 
            // btnShowWaitingList
            // 
            this.btnShowWaitingList.Location = new System.Drawing.Point(223, 18);
            this.btnShowWaitingList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnShowWaitingList.Name = "btnShowWaitingList";
            this.btnShowWaitingList.Size = new System.Drawing.Size(157, 27);
            this.btnShowWaitingList.TabIndex = 8;
            this.btnShowWaitingList.Text = "Show Waiting List";
            this.btnShowWaitingList.UseVisualStyleBackColor = true;
            this.btnShowWaitingList.Click += new System.EventHandler(this.btnShowWaitingList_Click);
            // 
            // btnShowAll
            // 
            this.btnShowAll.Location = new System.Drawing.Point(14, 18);
            this.btnShowAll.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(157, 27);
            this.btnShowAll.TabIndex = 7;
            this.btnShowAll.Text = "Show All";
            this.btnShowAll.UseVisualStyleBackColor = true;
            this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(65, 316);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Output";
            // 
            // btnDebug
            // 
            this.btnDebug.Location = new System.Drawing.Point(476, 328);
            this.btnDebug.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDebug.Name = "btnDebug";
            this.btnDebug.Size = new System.Drawing.Size(173, 133);
            this.btnDebug.TabIndex = 5;
            this.btnDebug.Text = "Fill All (Debug)";
            this.btnDebug.UseVisualStyleBackColor = true;
            this.btnDebug.Click += new System.EventHandler(this.btnDebug_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(80, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(133, 26);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 114);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(43, 171);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(43, 62);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(43, 224);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(43, 277);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "4";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 661);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnDebug);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "ZY Airline Reservation";
            this.Load += new System.EventHandler(this.frmAirlineReservation_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtState;
        private System.Windows.Forms.Button btnState;
        private System.Windows.Forms.Button btnAddWaitingList;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.ListBox lstColumn;
        private System.Windows.Forms.ListBox lstRow;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox rtbShowWaitingList;
        private System.Windows.Forms.RichTextBox rtbShowAll;
        private System.Windows.Forms.Button btnShowWaitingList;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDebug;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}

