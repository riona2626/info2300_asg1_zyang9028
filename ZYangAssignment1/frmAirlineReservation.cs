﻿/*
 * Assignment 1: Airline Reservation
 * Written By: Zixuan Yang
 * Student Number: 7109028
 * Date: May.31/2018
 * 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZYangAssignment1
{
    public partial class frmAirlineReservation : Form
    {
        public frmAirlineReservation()
        {
            InitializeComponent();
        }

        //set seat row & column
        const int row = 5;
        const int column = 3;

        string name;
        int check;

        bool state;
        int listId;

        //Add a seat button array
        Button[,] buttons = new Button[row , column];

        //Add waiting list array
        string[] waitingList = new string[10];

        //Generate seat buttons
        void GenerateButtons()
        {
            int x0 = 60, y0 =45, w = 50, d = 55;

            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < column; c++)
                {
                    Button btn = new Button();
                    btn.Top = y0 + r * d;
                    btn.Left = x0 + c * d;
                    btn.Width = w;
                    btn.Height = w;
                    btn.Visible = true;

                    //add a tag for button's row & column
                    btn.Tag = r * column + c;

                    //Register button click event
                    btn.Click += new EventHandler(btn_Click);

                    buttons[r, c] = btn;
                    this.Controls.Add(btn);
                }
            }
        }

        //Button click event.
        //Select the row and column in two lists.
        void btn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            lstRow.SelectedIndex = CalculateRow(btn);
            lstColumn.SelectedIndex = CalculateColumn(btn);
        }

        //Find button's row.
        int CalculateRow(Button btn)
        {
            int num = (int)btn.Tag;
            int r = num / column;

            return r;
        }

        //Find button's column.
        int CalculateColumn(Button btn)
        {
            int num = (int)btn.Tag;
            int c = num % column;

            return c;
        }

        //Generate buttons.
        //Select the first item in row list.
        //Select the first item in column list.
        private void frmAirlineReservation_Load(object sender, EventArgs e)
        {
            GenerateButtons();
            lstRow.SelectedIndex = 0;
            lstColumn.SelectedIndex = 0;
        }

        //Check all seat if available.
        //Return n = 0 for all seats are taken, other numbers for empty seat available.
        int checkAllSeat()
        {
            int n = 0;

            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < column; c++)
                {
                    if (checkAvailable(r,c) == true)
                    {
                        n++;
                    }
                }
            }
            return n;
        }

        //Check if waiting list is full.
        //Return n = i for empty slot, -1 for full.
        int CheckWaitingList()
        {
            int n = -1;

            for (int i = 0; i < waitingList.Length; i++)
            {
                if (waitingList[i] == null)
                {
                    n = i;
                    return n;
                }
            }
            return n;
        }

        //Swap the waiting list.
        //Move the items on index forward.
        void swapWaitingList()
        {
            string temp = null;

            for (int i = 0; i < waitingList.Length; i++)
            {
                if (i+1 < waitingList.Length)
                {
                    temp = waitingList[i];
                    waitingList[i] = waitingList[i + 1];
                    waitingList[i + 1] = temp;
                }
            }
        }

        //Check if the seat is available.
        //return true for available, false for unavailable.
        bool checkAvailable(int r, int c)
        {
            if (buttons[r , c].Text == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //Book button click event, to add name to select seat.
        //If all the seat is unavailable, add name to waiting list.
        //If waiting list is full, will not add.
        private void btnBook_Click(object sender, EventArgs e)
        {
            int r = int.Parse(lstRow.SelectedItem.ToString());
            int c = int.Parse(lstColumn.SelectedItem.ToString());

            name = txtName.Text;
            check = checkAllSeat();
            listId = CheckWaitingList();           
            state = checkAvailable(r, c);

            //Check if name input is vaild.
            if (name == "" || name == null)
            {
                MessageBox.Show("Please enter a name!", "Error Message:");
            }
            else
            {
                //Check if all seats are full.
                if (check != 0)
                {
                    //Check if selected seat is available.
                    if (state == true)
                    {
                        //Add name to seat.
                        buttons[r, c].Text = name;

                        MessageBox.Show("The seat [" + r + " , " + c + "] is now booked.", "Success!");
                    }
                    else
                    {
                        MessageBox.Show("Seat is occupied, please choose another one.", "Fail!");
                    }
                }
                else if(check == 0)
                {
                    //Check if waiting list is full.
                    if (listId != -1)
                    {
                        //Add name to waiting list.
                        waitingList[listId] = name;
                        MessageBox.Show("There is no seat available right now.\nAdd name to waiting list.", "Alert!");
                    }
                    else if(listId == -1)
                    {
                        MessageBox.Show("The waiting list is full. Cannot add any more!", "Fail!");
                    }
                }
            }
        }

        //State button event, show if a seat is available or unavailable.
        private void btnState_Click(object sender, EventArgs e)
        {
            int r = int.Parse(lstRow.SelectedItem.ToString());
            int c = int.Parse(lstColumn.SelectedItem.ToString());

            state = checkAvailable(r, c);

            if (state == true)
            {
                txtState.Text = "Available";
            }
            else if (state  == false)
            {
                txtState.Text = "Unavailable";
            }
        }

        //Show waiting list button event, show the waiting list.
        private void btnShowWaitingList_Click(object sender, EventArgs e)
        {
            string output = "";

            for (int i = 0; i < waitingList.Length; i++)
            {
                output += "[" + i + "] -- " + waitingList[i] + "\n";
                rtbShowWaitingList.Text = output;
            }
        }

        //Show all button event, show all the names of seats.
        private void btnShowAll_Click(object sender, EventArgs e)
        {
            string output = "";

            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < column; c++)
                {
                    output += "[" + r + "," + c + "] -- " + buttons[r, c].Text + "\n";
                    rtbShowAll.Text = output;
                }
            }
        }

        //Fill all button event, to input same information to all the seats.
        private void btnDebug_Click(object sender, EventArgs e)
        {
            string name = "Test";

            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < column; c++)
                {
                    buttons[r, c].Text = name;
                }
            }
        }

        //Add to Waiting list button event, to add name to waiting list.
        //If there are any seats available do nothing, and show message box.
        //if the waiting list is full, will not add and show message box.
        private void btnAddWaitingList_Click(object sender, EventArgs e)
        {
            check = checkAllSeat();
            listId = CheckWaitingList();
            name = txtName.Text;

            if (check == 0)
            {
                if (listId != -1)
                {
                    if (name == "" || name == null)
                    {
                        MessageBox.Show("Please enter a name!", "Error Message:");
                    }
                    else
                    {
                        waitingList[listId] = name;
                        MessageBox.Show("Successfully add name to waiting list!", "Success!");
                    }
                }
                else if(listId == -1)
                {
                    MessageBox.Show("The waiting list is full. Cannot add any more!", "Fail!");
                }
            }
            else if (check != 0)
            {
                MessageBox.Show("There are still some seats available. Cannot add name to waiting list!", "Fail!");
            }
        }

        //Cancel button event, to delete the name from seat, and add the first waiting list name to empty seat.
        //Move the rest of the items one index forward.
        private void btnCancel_Click(object sender, EventArgs e)
        {
            int r = int.Parse(lstRow.SelectedItem.ToString());
            int c = int.Parse(lstColumn.SelectedItem.ToString());
            state = checkAvailable(r, c);

            buttons[r, c].Text = "";

            if (r == 0 || c == 0)
            {
                MessageBox.Show("Please select a seat!");
            }
            else
            {
                if (state == true)
                {
                    MessageBox.Show("This seat has not been booked!");
                }
                else
                {
                    if (waitingList[0] != null)
                    {
                        name = waitingList[0];
                        buttons[r, c].Text = name;
                        waitingList[0] = null;
                        swapWaitingList();
                        MessageBox.Show("Successfully Cancelled and move the first person in the waiting list to the available seat! ");
                    }
                    else
                    {
                        MessageBox.Show("Successfully Cancelled! ");
                    }
                }
                
            }
        }

        //Set textbox only characters, space & backspace allowed
        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || (Keys)e.KeyChar == Keys.Space|| (Keys)e.KeyChar == Keys.Back)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
